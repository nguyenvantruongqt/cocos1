import { _decorator, Component, director, EventTouch, Input, Node, tween, UIOpacity } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('world')
export class world extends Component {
    backBtn: Node;
    protected onLoad(): void {
    }
    start() {
        this.node.getComponent(UIOpacity).opacity = 0;
        tween(this.node.getComponent(UIOpacity))
            .to(0.5, { opacity: 255 })
            .start();
        this.backBtn = this.node.getChildByName("BackBtn");
        this.backBtn.on(Input.EventType.TOUCH_END, this.onClickBackBtn, this);

    }

    update(deltaTime: number) {

    }

    onClickBackBtn(event: EventTouch) {
        tween(this.node.getComponent(UIOpacity))
            .to(0.5, { opacity: 0 })
            .call(() => { director.loadScene("main") })
            .start()
    }
}

