import { _decorator, Collider2D, Color, Component, Contact2DType, Graphics, IPhysics2DContact } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Slot')
export class Slot extends Component {
    start() {
        let graphics = this.node.getComponent(Graphics);
        if (graphics) {
            this.node.getComponent(Graphics).strokeColor = Color.YELLOW;
            this.node.getComponent(Graphics).lineWidth = 2;
            this.node.getComponent(Graphics).rect(0, 0, 200, 200);
            this.node.getComponent(Graphics).stroke();
        }
        let collider2D = this.node.getComponent(Collider2D);
        if (collider2D) {
            collider2D.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this);
        }
    }

    onBeginContact(selfCollider: Collider2D, otherCollider: Collider2D, contact: IPhysics2DContact) {
        // console.log('onBeginContact selfCollider: ', selfCollider.name)
        // console.log('onBeginContact otherCollider: ', otherCollider.name)
    }

    update(deltaTime: number) {

    }
}

