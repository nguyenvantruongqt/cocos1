import { _decorator, Component, director, EventMouse, Input, Node, tween, UIOpacity } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('main')
export class main extends Component {
    public nodeName: String = "";
    private playBtn: Node;
    private quitBtn: Node;

    protected start(): void {
        this.playBtn = this.node.getChildByName("Play")
        this.quitBtn = this.node.getChildByName("Quit")
        this.node.getComponent(UIOpacity).opacity = 255;
        this.playBtn.on(Input.EventType.MOUSE_DOWN, this.onClickPlayBtn, this);
        this.quitBtn.on(Input.EventType.MOUSE_DOWN, this.onClickQuitBtn, this);
        // director.loadScene("world")
    }

    update(deltaTime: number) {

    }

    onClickPlayBtn(event: EventMouse) {
        tween(this.node.getComponent(UIOpacity))
            .to(0.5, { opacity: 0 })
            .call(() => {
                director.loadScene("world")
            })
            .start()
    }

    onClickQuitBtn() {
        console.log('onClickQuitBtn')
    }
}

