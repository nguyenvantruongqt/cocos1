import { _decorator, Animation, AudioClip, AudioSource, Collider2D, Component, Contact2DType, EventTouch, Input, IPhysics2DContact, screen, tween, v3, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('Character')
export class Character extends Component {
    @property(AudioClip)
    soundYeah: AudioClip;
    @property(AudioClip)
    soundNo: AudioClip;
    audioSource: AudioSource;
    originalPosition: Vec3;

    start() {
        this.originalPosition = this.node.getPosition();
        this.node.on(Input.EventType.TOUCH_START, this.onTouchStart, this);
        this.node.on(Input.EventType.TOUCH_MOVE, this.onTouchMove, this);
        let collider2D = this.node.getComponent(Collider2D);
        if (collider2D) {
            collider2D.on(Contact2DType.BEGIN_CONTACT, this.onBeginContact, this)
        }
        this.audioSource = this.node.getComponent(AudioSource);
    }

    onTouchStart(event: EventTouch) {
        tween(this.node)
            .to(0.1, {
                position: v3(event.getLocation().x - screen.windowSize.width / 2, event.getLocation().y - screen.windowSize.height / 2, 0),
            })
            .start()
    }
    onTouchMove(event: EventTouch) {
        this.node.setPosition(v3(this.node.position.x + event.getDeltaX(), this.node.position.y + event.getDeltaY(), 0));
    }

    onBeginContact(me: Collider2D, other: Collider2D, contact: IPhysics2DContact) {
        if (me.tag === other.tag) {
            me.node.getComponent(Animation).play(me.node.name + "Jump");
            this.audioSource.playOneShot(this.soundYeah);
            // me.node.off(Input.EventType.TOUCH_START);
            // me.node.off(Input.EventType.TOUCH_MOVE);
            tween(me.node)
                .to(0.2, {
                    position: other.node.getPosition().add(v3(100, 100, 0)),
                })
                .call(() => {
                    me.node.getComponent(Animation).play(me.node.name + "Idle");
                })
                .start()
        }
        else {
            this.audioSource.playOneShot(this.soundNo);
            tween(me.node)
                .to(0.2, {
                    position: this.originalPosition,
                })
                .start()
        }
    }
}

